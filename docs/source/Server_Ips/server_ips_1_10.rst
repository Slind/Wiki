=================
Server Ip's 1.8 +
=================
.. contents:: Servers
  :depth: 2
  :local:

Anarchy (1.10.2)
^^^^^^^^
.. note:: to gain access to this 1.10.2 test sever you need to read and accept the rules located here:https://mineyourmind.net/forum/threads/1-10-2-anarchy-server-experimental.19723/
* ``skyblock.mym.li`` - Version ``1.10.2``
